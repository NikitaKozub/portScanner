package Test;

import EJB.EJB;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class EjbTest implements Serializable {

    private String result;

    private EJB ejb;

    @Test
    public void accessScanner() {
        ejb = new EJB();
        result = ejb.accessScanner("www.youtube.com", 80);
        assertNotNull(result);
        assertEquals("Порт " + 80 + " открыт", result);

        result = ejb.accessScanner("www.youtube.com", 8);
        assertNotNull(result);
        assertEquals("Порт " + 8 + " закрыт", result);
    }
}
