package Client;

import EJB.EJB;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@SessionScoped
public class Client implements Serializable {
    private int serverPort;
    private String localhost;
    private List<Integer> result;
    private String resultTarget;

    public String getResultTarget() {
        return resultTarget;
    }

    public void setResultTarget(String resultTarget) {
        this.resultTarget = resultTarget;
    }

    @javax.ejb.EJB
    private EJB ejb;

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getLocalhost() {
        return localhost;
    }

    public void setLocalhost(String localhost) {
        this.localhost = localhost;
    }

    public List<Integer> getResult() {
        return result;
    }

    public void setResult(List<Integer> result) {
        this.result = result;
    }

    public void accessScanner() {
        result = ejb.accessScanner(localhost);
    }

    public void accessScannerTarger() {
        resultTarget = ejb.accessScanner(localhost, serverPort);
    }
}
