package EJB;

import javax.ejb.Stateless;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


@Stateless
public class EJB {
    private List portOpen = new ArrayList<Integer>();

    public List<Integer> accessScanner(String localhost) {
        Socket socket = null;
        int MAX_PORT = 65535;
        for (int port = 80; port < MAX_PORT; port++) {
            try {
                InetAddress ipAddress = InetAddress.getByName(localhost);
                socket = new Socket(ipAddress, port);
                if (socket.isConnected()) {
                    portOpen.add(port);
                }
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return portOpen;
    }

    public String accessScanner(String host, int port) {
        Socket socket = null;
        String result = "";
        try {
            socket = new Socket(host, port);
            if (socket.isConnected()) {
                result = "Порт " + port + " открыт";
            }
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
            result = "Порт " + port + " закрыт";
        }

        return result;
    }
}
